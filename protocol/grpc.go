package protocol

import (
	"fmt"
	"net"

	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"context"
	"google.golang.org/grpc"
)


func NewGrpc(ctxs  context.Context) *CmdbGrpc { 
	ctx,cancel:=context.WithCancel(ctxs)
	return &CmdbGrpc{
		ctx: ctx,
		cancel: cancel,
		server: grpc.NewServer(),
	}
}

type  CmdbGrpc struct {
	server *grpc.Server
	ctx  context.Context
	cancel context.CancelFunc
}



func (c *CmdbGrpc)Start() error {
	// 注册各自服务
	err:=ioc.InitGrpcService(c.server)
	if err != nil {
		return err
	}
	lis,err:=net.Listen("tcp",config.C.Grpc.GetAddress())
	if err != nil{
		return err
	}
	fmt.Printf("Start  grpc  service,listenning:%s\n",config.C.Grpc.GetAddress())
	return  c.server.Serve(lis)
}


func (c  *CmdbGrpc) Stop() error {
	c.server.GracefulStop()
	return  nil 
}