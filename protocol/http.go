package protocol

import (
	"context"
	"fmt"
	"net/http"

	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/gin-gonic/gin"
)


type  CmdbHttps struct{
	server *http.Server
	router *gin.Engine
	ctx  context.Context
	cancel context.CancelFunc
}


func NewHttpServer(ctxs context.Context) *CmdbHttps {
	r:=gin.Default()
	ctx,cancel:=context.WithCancel(ctxs)
	return &CmdbHttps{
		ctx: ctx,
		cancel: cancel,
		router: r,
		server: &http.Server{
			// 服务监听的地址
			Addr: config.C.Http.GetAddress(),
			// 监听关联的路由处理
			Handler: r,
		},
	}
}


func  (c  *CmdbHttps) HelloWorld(r *gin.Context) {
	r.JSON(200, "Hello World")
	return
}

// 服务启动
func(c *CmdbHttps) Start()  error {

	v1:=c.router.Group("/api/v1")
	v1.GET("/index", c.HelloWorld)
	ioc.InitHttpService(v1)
	fmt.Printf("Start  http  service,listenning:%s\n",c.server.Addr)
	return c.server.ListenAndServe()
}

func (c *CmdbHttps) ShutDown(ctx context.Context) error {
	return c.server.Shutdown(ctx)
}