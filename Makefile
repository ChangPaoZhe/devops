PKG := "gitee.com/ChangPaoZhe/devops"


runcmdb: ## Run Cmdb
	@ cd apps/cmdb && go build -o cmdb main.go && ./cmdb

runma: ## Run Maudit
	@ cd command && go run main.go

install: ## Install depence go package
	@/usr/local/go/bin/go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	@/usr/local/go/bin/go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
	@/usr/local/go/bin/go install github.com/favadi/protoc-go-inject-tag@latest

gen: ## protobuf 编译
	@protoc -I=. -I=/usr/local/include --go_out=. --go_opt=module=${PKG} --go-grpc_out=. --go-grpc_opt=module=${PKG} apps/*/pb/*.proto
	@/root/go/bin/protoc-go-inject-tag -input=apps/*/*.pb.go

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'