package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitee.com/ChangPaoZhe/devops/apps/cron"
	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"gitee.com/ChangPaoZhe/devops/protocol"
	_ "gitee.com/ChangPaoZhe/devops/registory"
)


var  (
	wg  sync.WaitGroup
	ctx context.Context
	cancle context.CancelFunc
	HttpServer  *protocol.CmdbHttps
	GrpcServer *protocol.CmdbGrpc
)

// 服务停止运行的逻辑
// 1 通过 signal.Notify(c, syscall.SIGINT, syscall.SIGTERM) 接收监听的信号，当出现信号匹配时会写入对应struct
// 2 通过for 加 select 形成监听，用于监听信号的结束，若结束信号发起，则会同步到公共CTX中，通过调用其函数完成对其他信号的终止逻辑
// 3 通过ctx.Done() 完成最终的服务结束
// 4 通过wg 进行统计当前启动的线程数量，用于计数，并最终释放。Add创建  Wait 等待  Done 减关闭


// 监听并终止进程
func ListenSinal(){
	c:=make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM,syscall.SIGKILL)
	for {
		select {
		case sig:=<-c:
			fmt.Printf("关闭动作 %d\n",sig)
			if err:=HttpServer.ShutDown(ctx);err!=nil {
				panic(err.Error())
			}
			fmt.Println("关闭Http服务成功")
			wg.Done()
			if err:=GrpcServer.Stop();err!=nil  {
				panic(err)
			}
			fmt.Println("关闭Grpc服务成功")
			wg.Done()
			cancle()
			config.EndInit(ctx)
			return
		}
	
	}
}

func main() {

	// 调用配置模块，完成配置更新
	fp:="/home/jasonbzhang/work/script/golang/devops/etc/application.toml"
	err := config.LoadConfigFromToml(fp)
	if err != nil {
		panic(err)
	}
	// 调用常规服务，用于注册和生成基础模块
	err=ioc.InitService()
	if err != nil {
		panic(err.Error())
	}
	HttpServer=protocol.NewHttpServer(ctx)
	GrpcServer=protocol.NewGrpc(ctx)
	go  func(){
		wg.Add(1)
		GrpcServer.Start()
	}()
	go func(){
		wg.Add(1)
		HttpServer.Start()
	
	}()
	// 增加crontab 功能
	if config.C.CrontabStatus.Enable==true {
		cronImpl:=ioc.GetServer(cron.App_Name).(cron.Croner)
		cronImpl.Start()
	}
	ListenSinal()
	wg.Wait()  // 进行卡进程
}
func init(){
	wg=sync.WaitGroup{}
	ctx, cancle = context.WithCancel(context.Background()) //父context
}