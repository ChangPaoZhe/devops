package main 

import (
	"math/rand"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)


type  HttpResponseCollector  struct {
	HttpresponseStatus *prometheus.GaugeVec
	
}


// 初始化函数
func NewDefaultHttpResponseCollector()  *HttpResponseCollector {
	return &HttpResponseCollector{
		HttpresponseStatus: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name:"Http_Response",
			Help: "Current Http Response",
		}, []string{"http_status"}),
	}
}



func  (c *HttpResponseCollector)  Describe(ch chan<-*prometheus.Desc){
	c.HttpresponseStatus.Describe(ch)
}


func (c *HttpResponseCollector)  Collect(ch chan<- prometheus.Metric){
	c.HttpresponseStatus.WithLabelValues("kitchen").Set(rand.Float64() * 100) // 设置table 和值
	c.HttpresponseStatus.WithLabelValues("bedroom").Set(rand.Float64() * 100)
	c.HttpresponseStatus.Collect(ch)
}


func main() {
	// Create a new instance of the custom collector.
	collector := NewDefaultHttpResponseCollector()

	// Register the custom collector with the Prometheus registry.
	prometheus.MustRegister(collector)

	// Start a HTTP server to expose the metrics.
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":8080", nil)
}