package httpresponse

import (
	"net/http"
	"fmt"
	"github.com/gin-gonic/gin"
)

// 业务自定义异常，包括
type ApiExecption struct{
	// 内侧业务异常
	BizCode  int `json:"code"`
	// 状态码返回异常
	HttpCode  int `json:"http_code"`
	// 返回会话信息
	Message string `json:"message"`
	// 返回数据
	Data  any  `json:"data"`
}


// 初始化
// New(5000,"令牌过期")
func  New(code  int,format string,a ...any)  *ApiExecption {
	httpCode:=http.StatusInternalServerError
	if code/100<6 && code/100>0 {
		httpCode=code
	}
	return  &ApiExecption{
		BizCode: code,
		Message: fmt.Sprintf(format, a...),
		HttpCode: httpCode,
	}
}


// 其实现了error 的方法
func (e *ApiExecption) Error() string{
	return e.Message
}



// 定义异常
func NewNotFound(format  string,a  ...any) *ApiExecption{
	return	 New(400,format,a...)
}

func IsNotFound(err error) bool {
	if e,ok:=err.(*ApiExecption);ok { // 断言error是否满足当前方法 
		if e.BizCode==404 {
			return  true
		}
	}
	return false
}

func NewAuthFailed(format  string,a ...any)  *ApiExecption {
	return New(5000, format, a...)
}



func NewPermissionDeny(format  string,a ...any) *ApiExecption {
	return New(5001, format, a...)
}

func NewTokenExpired(format  string,a ...any) *ApiExecption {
	return New(5002, format, a...)
}

func NewParamError(format  string,a ...any) *ApiExecption {
	return New(5003, format, a...)
}


func Success(c *gin.Context, data any) {
	c.JSON(http.StatusOK, gin.H{
		"msg": "ok",
		"data": data,
	})
}

// 异常情况的数据返回, 返回我们的业务Exception
func Failed(c *gin.Context, err error) {
	// 如果出现多个Handler， 需要通过手动abord
	defer c.Abort()
	var e *ApiExecption
	if v, ok := err.(*ApiExecption); ok {
		
		e = v
	} else {
		// 非可以预期, 没有定义业务的情况
		e = New(http.StatusInternalServerError, err.Error())
		e.HttpCode = http.StatusInternalServerError
	}

	c.JSON(e.HttpCode, e)
}