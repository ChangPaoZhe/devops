package config

import (
	"context"
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

// 初始化环境
var (
	// 全局变量
	C *Config =NewConfigDefault()
)


// 负责加载配置
func LoadConfigFromToml(filepath string) error {
	// 文件里面的toml格式的数据 转换为一个 Config对象
	_, err := toml.DecodeFile(filepath, C)
	if err != nil {
		return err
	}
	return nil
}

// 负责加载配置
func LoadConfigFromEnv() error {
	// 完成环境变量与Config对象的映射
	return env.Parse(C)
}

// 释放连接
func EndInit(ctx  context.Context){
	for  {
		select{
		case <-ctx.Done():
			for _,fn:=range  C.Logs.FnList {
				fn.Close()
			}
		}
		return 
	}
	
 }