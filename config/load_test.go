package config_test

import (
	"fmt"
	"testing"

	"gitee.com/ChangPaoZhe/devops/config"
)

func TestLoadConfigFromToml(t *testing.T) {
	fp:="/home/jasonbzhang/work/script/golang/devops/etc/application.toml"
	err := config.LoadConfigFromToml(fp)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(config.C)
}