package ioc

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

var  (
	service_map=map[string]Servicer{}
	serviceHttp_map=map[string]HttpService{}
	serviceGrpc_map=map[string]GrpcService{}

)

// 用于注册和初始化资源

type  Servicer  interface {
	// 返回资源名称
	Name() string;
	// 定义初始化函数
	Init() error;
}


type HttpService interface{
	Registory(gin.IRouter);
	Servicer
}


type GrpcService interface {
	Servicer
	Registory(grpc.ServiceRegistrar)
}

// 注册资源
func  Register(svc  Servicer) {
	service_map[svc.Name()]=svc
}
func  RegisterHttp(svc  HttpService) {
	serviceHttp_map[svc.Name()]=svc
}

func RegisterGrpc(svc GrpcService) {
	serviceGrpc_map[svc.Name()]=svc
}
// 校验资源
func GetServer(name string) any  {
    if v,ok:=service_map[name];ok{
        return v
    }
    panic(fmt.Sprintf("%s 未注册",name))
}
// 初始化服务
func InitService() error {
	for  name:=range service_map{
		servicer:=service_map[name]
		if err:=servicer.Init();err!=nil {
			return err 
		}
	}
	return nil 
}


func InitHttpService(r   gin.IRouter) error{
	for  name:=range serviceHttp_map{
		service_http:=serviceHttp_map[name]
		if err:=service_http.Init();err!=nil {
			return err 
		}
		service_http.Registory(r)  // 服务注册
	}
	return nil 
}


func InitGrpcService(r   *grpc.Server) error{
	for  name:=range serviceGrpc_map{
		service_grpc:=serviceGrpc_map[name]
		if err:=service_grpc.Init();err!=nil {
			return err 
		}
		service_grpc.Registory(r)
	}
	return nil 
}