package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/user/impl"
	"gitee.com/ChangPaoZhe/devops/httpresponse"
	"github.com/gin-gonic/gin"
)


func (u *UserHttpImpl)  Register(c  *gin.Context){
	req:=impl.NewDefaultRegisterRequest()
	err:=c.BindJSON(req)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	users,err:=u.Svc.Register(c.Request.Context(), req)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(c,users)
}
func (u *UserHttpImpl) CheckPassword(c  *gin.Context) {
	req:=impl.NewDefaultCheckPasswordRequest()
	err:=c.BindJSON(req)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	users,err:=u.Svc.CheckPassword(c.Request.Context(), req)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(c,users)
}
func (u *UserHttpImpl)   Search(c  *gin.Context) {
	req:=impl.NewDefaultSearchRequest()
	err:=c.BindJSON(req)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	users,err:=u.Svc.Search(c.Request.Context(), req)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(c,users)
}