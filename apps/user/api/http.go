package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/user"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/gin-gonic/gin"
)

type  UserHttpImpl  struct {
	Svc  user.Userer
}


func  (u *UserHttpImpl)  Name() string {
	return  user.App_Name
}

func  (u *UserHttpImpl) Init() error  {
	u.Svc=ioc.GetServer(user.App_Name).(user.Userer)
	return nil  
}
func  (u *UserHttpImpl) Registory(r  gin.IRouter) {
	v1:=r.Group(user.App_Name)
	v1.POST("/register", u.Register)
	v1.POST("/checkpassword", u.CheckPassword)
	v1.POST("/search", u.Search)
}

func init(){
	ioc.RegisterHttp(&UserHttpImpl{})
}