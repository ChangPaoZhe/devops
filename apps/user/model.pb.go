// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.12.4
// source: apps/user/pb/model.proto

package user

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Role int32

const (
	Role_GUEST Role = 0
	Role_USER  Role = 1
	Role_ADMIN Role = 2
)

// Enum value maps for Role.
var (
	Role_name = map[int32]string{
		0: "GUEST",
		1: "USER",
		2: "ADMIN",
	}
	Role_value = map[string]int32{
		"GUEST": 0,
		"USER":  1,
		"ADMIN": 2,
	}
)

func (x Role) Enum() *Role {
	p := new(Role)
	*p = x
	return p
}

func (x Role) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Role) Descriptor() protoreflect.EnumDescriptor {
	return file_apps_user_pb_model_proto_enumTypes[0].Descriptor()
}

func (Role) Type() protoreflect.EnumType {
	return &file_apps_user_pb_model_proto_enumTypes[0]
}

func (x Role) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Role.Descriptor instead.
func (Role) EnumDescriptor() ([]byte, []int) {
	return file_apps_user_pb_model_proto_rawDescGZIP(), []int{0}
}

type User struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 自增ID
	// @gotags: bson:"_id,omitempty" json:"id"
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id" bson:"_id,omitempty"`
	// 用户名
	//@gotags: json:"username" bson:"username"
	Username string `protobuf:"bytes,2,opt,name=username,proto3" json:"username" bson:"username"`
	// 电话号码
	//@gotags: json:"phone_number" bson:"phone_number"
	PhoneNumber string `protobuf:"bytes,3,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number" bson:"phone_number"`
	// 住址
	//@gotags: json:"address" bson:"address"
	Address string `protobuf:"bytes,4,opt,name=address,proto3" json:"address" bson:"address"`
	// 用户角色
	// @gotags: json:"role" bson:"role"
	Role Role `protobuf:"varint,5,opt,name=role,proto3,enum=user.Role" json:"role" bson:"role"`
	// 哈希权限
	// @gotags: json:"hash_passwd" bson:"hash_passwd"
	HashPasswd string `protobuf:"bytes,6,opt,name=hash_passwd,json=hashPasswd,proto3" json:"hash_passwd" bson:"hash_passwd"`
	// 创建时间
	//@gotags: json:"create_at" bson:"create_at"
	CreateAt int64 `protobuf:"varint,7,opt,name=create_at,json=createAt,proto3" json:"create_at" bson:"create_at"`
}

func (x *User) Reset() {
	*x = User{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_user_pb_model_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *User) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*User) ProtoMessage() {}

func (x *User) ProtoReflect() protoreflect.Message {
	mi := &file_apps_user_pb_model_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use User.ProtoReflect.Descriptor instead.
func (*User) Descriptor() ([]byte, []int) {
	return file_apps_user_pb_model_proto_rawDescGZIP(), []int{0}
}

func (x *User) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *User) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *User) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *User) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *User) GetRole() Role {
	if x != nil {
		return x.Role
	}
	return Role_GUEST
}

func (x *User) GetHashPasswd() string {
	if x != nil {
		return x.HashPasswd
	}
	return ""
}

func (x *User) GetCreateAt() int64 {
	if x != nil {
		return x.CreateAt
	}
	return 0
}

var File_apps_user_pb_model_proto protoreflect.FileDescriptor

var file_apps_user_pb_model_proto_rawDesc = []byte{
	0x0a, 0x18, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x75, 0x73, 0x65, 0x72, 0x2f, 0x70, 0x62, 0x2f, 0x6d,
	0x6f, 0x64, 0x65, 0x6c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x75, 0x73, 0x65, 0x72,
	0x22, 0xcd, 0x01, 0x0a, 0x04, 0x55, 0x73, 0x65, 0x72, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65,
	0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65,
	0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e,
	0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f,
	0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x12, 0x1e, 0x0a, 0x04, 0x72, 0x6f, 0x6c, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0e,
	0x32, 0x0a, 0x2e, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x52, 0x6f, 0x6c, 0x65, 0x52, 0x04, 0x72, 0x6f,
	0x6c, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x68, 0x61, 0x73, 0x68, 0x5f, 0x70, 0x61, 0x73, 0x73, 0x77,
	0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x68, 0x61, 0x73, 0x68, 0x50, 0x61, 0x73,
	0x73, 0x77, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x5f, 0x61, 0x74,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x74,
	0x2a, 0x26, 0x0a, 0x04, 0x52, 0x6f, 0x6c, 0x65, 0x12, 0x09, 0x0a, 0x05, 0x47, 0x55, 0x45, 0x53,
	0x54, 0x10, 0x00, 0x12, 0x08, 0x0a, 0x04, 0x55, 0x53, 0x45, 0x52, 0x10, 0x01, 0x12, 0x09, 0x0a,
	0x05, 0x41, 0x44, 0x4d, 0x49, 0x4e, 0x10, 0x02, 0x42, 0x28, 0x5a, 0x26, 0x67, 0x69, 0x74, 0x65,
	0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x43, 0x68, 0x61, 0x6e, 0x67, 0x50, 0x61, 0x6f, 0x5a, 0x68,
	0x65, 0x2f, 0x64, 0x65, 0x76, 0x6f, 0x70, 0x73, 0x2f, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x75, 0x73,
	0x65, 0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_apps_user_pb_model_proto_rawDescOnce sync.Once
	file_apps_user_pb_model_proto_rawDescData = file_apps_user_pb_model_proto_rawDesc
)

func file_apps_user_pb_model_proto_rawDescGZIP() []byte {
	file_apps_user_pb_model_proto_rawDescOnce.Do(func() {
		file_apps_user_pb_model_proto_rawDescData = protoimpl.X.CompressGZIP(file_apps_user_pb_model_proto_rawDescData)
	})
	return file_apps_user_pb_model_proto_rawDescData
}

var file_apps_user_pb_model_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_apps_user_pb_model_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_apps_user_pb_model_proto_goTypes = []interface{}{
	(Role)(0),    // 0: user.Role
	(*User)(nil), // 1: user.User
}
var file_apps_user_pb_model_proto_depIdxs = []int32{
	0, // 0: user.User.role:type_name -> user.Role
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_apps_user_pb_model_proto_init() }
func file_apps_user_pb_model_proto_init() {
	if File_apps_user_pb_model_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_apps_user_pb_model_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*User); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_apps_user_pb_model_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_apps_user_pb_model_proto_goTypes,
		DependencyIndexes: file_apps_user_pb_model_proto_depIdxs,
		EnumInfos:         file_apps_user_pb_model_proto_enumTypes,
		MessageInfos:      file_apps_user_pb_model_proto_msgTypes,
	}.Build()
	File_apps_user_pb_model_proto = out.File
	file_apps_user_pb_model_proto_rawDesc = nil
	file_apps_user_pb_model_proto_goTypes = nil
	file_apps_user_pb_model_proto_depIdxs = nil
}
