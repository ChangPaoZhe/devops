package impl

import (
	"gitee.com/ChangPaoZhe/devops/apps/user"
	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/mongo"
	"gitee.com/ChangPaoZhe/devops/apps/token"

)


type UserImpl  struct {
	logger zerolog.Logger
	user.UnsafeRPCServer
	MogoDb   *mongo.Database
}

func  (u *UserImpl)  Name() string {
	return  user.App_Name
}

func  (u *UserImpl) Init() error  {
	u.MogoDb=config.C.MongoDb.GetMongoDbInit().Database(token.App_Name)
	u.logger=config.C.Getlog(token.App_Name)
	return nil  
}


func init(){
	ioc.Register(&UserImpl{})
}