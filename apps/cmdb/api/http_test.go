package api_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"
	"gitee.com/ChangPaoZhe/devops/apps/cmdb/api"
)

func TestSearch(t *testing.T) {
	url:="http://localhost:8090/api/v1/search"
	req,err:=http.NewRequest("POST", url, nil)
	if err != nil {
		t.Fatal(err)
		return  
	}
	
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal("Error sending HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	// 读取响应内容
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Error reading response body:", err)
		return
	}

	// 输出响应内容
	t.Log("Response status:", resp.Status)
	t.Log("Response body:", string(body))
}


func TestRegister(t *testing.T){
	url:="http://localhost:8090/api/v1/v1/cmdb/registry"
	ins:=api.NewDefaultRegisterRequest()
	ins.Key="Mysql#1234"
	ins.Resource.Ipaddress="192.168.1.100"
	ins.Resource.SystemName="linux"
	bodys,err:=json.Marshal(ins)
	if err != nil {
		t.Fatal(err)
	}
	req,err:=http.NewRequest("POST", url,  bytes.NewBuffer(bodys))
	if err != nil {
		t.Fatal(err)
		return  
	}
	
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal("Error sending HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	// 读取响应内容
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Error reading response body:", err)
		return
	}

	// 输出响应内容
	t.Log("Response status:", resp.Status)
	t.Log("Response body:", string(body))
}