package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"gitee.com/ChangPaoZhe/devops/apps/user"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"gitee.com/ChangPaoZhe/devops/middleware"
	"github.com/gin-gonic/gin"
)



type CmdbHttp  struct {
	// 集成接口，可以调用接口方法
	svc    cmdb.RPCClienter		
}



func(c *CmdbHttp)Name() string {
	return  cmdb.App_Name
}

func (c *CmdbHttp) Init() error {
	// 初始化方法
	c.svc = ioc.GetServer(cmdb.App_Name).(cmdb.RPCClienter)
	return nil 
}
func (c *CmdbHttp)Registory(r gin.IRouter){
	v1:=r.Group("cmdb")
	// 后台管理接口，需要认证
	v1.Use(middleware.NewTokenAuther().Auth)
	v1.POST("/registry", middleware.Required(user.Role_ADMIN),c.Register)
	v1.POST("/search", middleware.Required(user.Role_GUEST),c.Search)
}

func init(){
	ioc.RegisterHttp(&CmdbHttp{})
}

