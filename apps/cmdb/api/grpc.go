package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"google.golang.org/grpc"
	"context"
)
type CmdbGrpc   struct {
	// 集成接口，可以调用接口方法
	cmdb.UnimplementedRPCServer
	svc  cmdb.RPCClienter

}


func (c *CmdbGrpc)Name() string {
	return cmdb.App_Name
}

func (c *CmdbGrpc)Init() error {
	c.svc=ioc.GetServer(cmdb.App_Name).(cmdb.RPCClienter)
	return nil  
}

func (c *CmdbGrpc)Registory(r  grpc.ServiceRegistrar) {
	// 注册服务
	cmdb.RegisterRPCServer(r, c)
}

func (c *CmdbGrpc) Register(ctx  context.Context,req  *cmdb.RegisterRequest) (*cmdb.Resource, error) {
	return  c.svc.Register(ctx, req)

}
func (c *CmdbGrpc) Search(ctx  context.Context,req   *cmdb.SearchRequest) (*cmdb.SearchReponseResource, error)  {
	// 通过key 字段查询当前实例详情
	return  c.svc.Search(ctx, req)
}
func (c *CmdbGrpc) Healther(ctx  context.Context,req *cmdb.RequestHealth) (*cmdb.HealthSave, error)  {
	return  c.svc.Healther(ctx, req)
}
func init(){
	ioc.RegisterGrpc(&CmdbGrpc{})
}