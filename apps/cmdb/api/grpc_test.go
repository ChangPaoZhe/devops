package api_test

import (
	"context"
	"testing"
	"time"

	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)
func NewDefaultRegisterRequest()  *cmdb.RegisterRequest{
	return  &cmdb.RegisterRequest{
		Key: "Mysql#1234",
		Resource: &cmdb.Resource{
			Ipaddress: "192.168.1.250",
			SystemName: "linux",
			Region: "sh",
			Create_At: time.Now().Unix(),
		},
	}
}

func  NewDefaultSearchRequest()  *cmdb.SearchRequest{
	return  &cmdb.SearchRequest{

	}
}
func TestGrpcRegister(t *testing.T)  {
	conn,err:=grpc.Dial("localhost:9090",grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	client:=cmdb.NewRPCClient(conn)
	resp,err:=client.Register(context.Background(),NewDefaultRegisterRequest())
	if  err != nil {
		t.Fatal(err.Error())
	}
	t.Log(resp)
}

func TestGrpcSearch(t *testing.T)  {
	conn,err:=grpc.Dial("localhost:9090",grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	req:=NewDefaultSearchRequest()
	// req.Region="gz"
	req.Ipaddress="192.168.1.250"
	req.SystemName="linux"
	client:=cmdb.NewRPCClient(conn)
	resp,err:=client.Search(context.Background(),req)
	if  err != nil {
		t.Fatal(err.Error())
	}
	t.Log("-------------------",resp)
}