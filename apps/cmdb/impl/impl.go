package impl

import (
	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/mongo"
)
type CmdbImpl struct {
	MongoDb  *mongo.Database
	cmdb.UnsafeRPCServer
	logger  zerolog.Logger
}


func (c *CmdbImpl)Name() string {
	return  cmdb.App_Name 
}

func (c *CmdbImpl)Init() error {
	c.MongoDb=config.C.MongoDb.GetMongoDbInit().Database(cmdb.App_Name)
	c.logger=config.C.Getlog(cmdb.App_Name)
	return nil 
}


func init(){
	ioc.Register(&CmdbImpl{})
}