package impl_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitee.com/ChangPaoZhe/devops/apps/token"
	"gitee.com/ChangPaoZhe/devops/apps/token/impl"
	"gitee.com/ChangPaoZhe/devops/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var  (
	Svc  = &impl.TokenImpl{}
	ctx = context.Background()
)


type User struct {
	// 自增ID
	// @gotags: bson:"_id,omitempty" json:"_id"
	Id primitive.ObjectID `protobuf:"bytes,3,opt,name=id,proto3" json:"_id" bson:"_id,omitempty"`
	// 标识地域
	// 用户名称
	// @gotags: json:"username" bson:"username"
	Username string `protobuf:"bytes,2,opt,name=username,proto3" json:"username" bson:"username"`
	// 用户密码
	// @gotags: json:"password" bson:"password"
	Password string `protobuf:"bytes,3,opt,name=password,proto3" json:"password" bson:"password"`
	// 创建时间
	// @gotags: json:"password" bson:"password"
	CreateAt  int64 `bson:"create_at"`
}

func NewDefaultUser()  *User{
	return  &User{
		CreateAt: time.Now().Unix(),
	}
}

func (u *User) String() string {
	return fmt.Sprintf("username:%s  password:%s  create_at:%d  Id:%s" ,u.Username,u.Password,u.CreateAt,u.Id)
}

func TestNewDefault(t *testing.T) {
	token:=impl.NewDefaultToken()
	t.Log(token.String())
}


func Test_Login(t *testing.T) {
	login:=impl.NewDefaultLoginRequest()
	login.Username="username"
	login.Password="password"
	token,err:=Svc.Login(ctx,login)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(token)
}


func TestNewCreateUser(t *testing.T){
	user:=NewDefaultUser()
	user.Username = "username"
	user.Password = "password"
	c:=config.C.GetMongoDbInit()
	collection:=c.Database(token.App_Name).Collection("user")
	result,err:=collection.InsertOne(ctx, user)  //插入数据
	if  err != nil{
		t.Fatal(err)
	}
	id := result.InsertedID.(primitive.ObjectID)
	filter := bson.M{"_id": id}
	var insertedUser *User
	err = collection.FindOne(ctx, filter).Decode(&insertedUser)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(insertedUser)
}

func TestLogout(t *testing.T)  {
	req:=impl.NewDefaultLogOutRequest()
	req.Accesstoken="c4812e03-affe-49e0-bae5-6de3a1745041"
	req.Refreshtoken="7ab8a5ca-a761-497c-8538-5e455db5144c"
	token,err:=Svc.Logout(ctx, req)
	if 	err != nil {
		t.Fatal(err)
	}
	t.Log(token)
} 


func TestValidateToken(t *testing.T) {
	req:=impl.NewDefaultRequestValidateToken()
	req.Accesstoken="c4812e03-affe-49e0-bae5-6de3a1745041"
	token,err:=Svc.ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(token)
}

func  init(){
	fp:="/home/jasonbzhang/work/script/golang/devops/etc/application.toml"
	err := config.LoadConfigFromToml(fp)
	if err != nil {
		panic(err)
	}
	Svc.MongDB = config.C.GetMongoDbInit().Database(token.App_Name)
}