package impl

import (
	"fmt"

	"gitee.com/ChangPaoZhe/devops/apps/token"
	"gitee.com/ChangPaoZhe/devops/apps/user"
	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/mongo"
)

type TokenImpl struct {
	MongDB *mongo.Database
	token.UnsafeRPCServer
	logger  zerolog.Logger
	user  user.Userer
}


func (t *TokenImpl)Init() error {
	fmt.Println("mongdb init")
	t.MongDB = config.C.GetMongoDbInit().Database(token.App_Name)
	t.logger=config.C.Getlog(token.App_Name)
	t.user=ioc.GetServer(user.App_Name).(user.Userer)
	return nil
}

func (t *TokenImpl)Name() string {
	return token.App_Name
}


func init() {
	ioc.Register(&TokenImpl{})
}