package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/token"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/gin-gonic/gin"
)
type TokenHttp struct {
	svc  token.Tokener
}

func (t *TokenHttp)Name() string {
	return  token.App_Name
}

func (t *TokenHttp)Init() error {
	t.svc=ioc.GetServer(token.App_Name).(token.Tokener)
	return nil
}

func (t *TokenHttp)Registory(g  gin.IRouter)   {
	v1:=g.Group("token")
	v1.POST("/login", t.Login)
	v1.POST("/logout", t.Logout)
	v1.POST("/check", t.ValidateToken)

}

func init(){
	ioc.RegisterHttp(&TokenHttp{})
}