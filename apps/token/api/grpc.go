package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/token"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"google.golang.org/grpc"
)
type TokenGrpc struct {
	svc  token.Tokener
	token.UnimplementedRPCServer
}

func (t *TokenGrpc)Name() string {
	return  token.App_Name
}

func (t *TokenGrpc)Init() error {
	t.svc=ioc.GetServer(token.App_Name).(token.Tokener)
	return nil 
}

func (t *TokenGrpc)Registory(g  grpc.ServiceRegistrar) {
	token.RegisterRPCServer(g, t)
}

func init(){
	ioc.RegisterGrpc(&TokenGrpc{})
}