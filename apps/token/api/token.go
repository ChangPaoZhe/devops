package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/token"
	"gitee.com/ChangPaoZhe/devops/httpresponse"
	"github.com/gin-gonic/gin"
	"gitee.com/ChangPaoZhe/devops/apps/token/impl"
)





func (t *TokenHttp)Login(c  *gin.Context) {
	login:=&token.LoginRequest{}
	err:=c.BindJSON(login)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	tokens,err:=t.svc.Login(c.Request.Context(), login)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	c.SetCookie(token.TOKEN_Cookie_NAME, tokens.Accesstoken, 0, "/", "0.0.0.0", false,true)
	httpresponse.Success(c,tokens)
}
func (t *TokenHttp) Logout(c  *gin.Context) {
	req:=impl.NewDefaultLogOutRequest()
	err:=c.BindJSON(req)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	ins,err:=t.svc.Logout(c.Request.Context(),req)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(c,ins)
}

	// 校验token，通过用户名和token进行校验
func (t *TokenHttp)	ValidateToken(c  *gin.Context){
	ins:=impl.NewDefaultRequestValidateToken()
	err:=c.BindJSON(ins)
	if err!=nil {
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return 
	}
	tokens,err:=t.svc.ValidateToken(c.Request.Context(), ins)
	if err!=nil{
		httpresponse.Failed(c,httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(c,tokens)
}