package impl

import (


	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"gitee.com/ChangPaoZhe/devops/apps/command"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/mongo"
	"gitee.com/ChangPaoZhe/devops/config"
)


type CommandImpl struct {
	mongDB  *mongo.Database
	command.UnsafeRPCServer
	logger zerolog.Logger
	CmdbSvc  cmdb.RPCClienter
}



func (c  *CommandImpl)Name() string {
	return command.App_Name
}

func (c  *CommandImpl) Init() error {
	c.mongDB = config.C.GetMongoDbInit().Database(command.App_Name)
	c.logger=config.C.Getlog(command.App_Name)
	// 初始化CMDB 模块
	c.CmdbSvc=ioc.GetServer(cmdb.App_Name).(cmdb.RPCClienter)
	return nil 
}

func init(){
	ioc.Register(&CommandImpl{})
}