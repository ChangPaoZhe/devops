package impl


import (
    "fmt"
    "github.com/robfig/cron"
)
func (c  *CommandImpl)CronExec() {
    cr := cron.New()
    cr.AddFunc("* * * * * *", func() {
        fmt.Println("Hello, world!")
    })
    cr.Start()

    // 防止主协程过早退出
    select {}
}