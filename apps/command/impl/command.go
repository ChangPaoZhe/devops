package impl

import (
	"context"
	"fmt"
	"time"

	cmdbimpl "gitee.com/ChangPaoZhe/devops/apps/cmdb/impl"
	"gitee.com/ChangPaoZhe/devops/apps/command"
	"gitee.com/ChangPaoZhe/devops/apps/command/util"
	"github.com/google/uuid"
)


func DefaultRandom()  string  {
	randomUUID, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}
	return   randomUUID.String()
}


func NewDefaultResponseStatus()  *command.ResponseStatus {
	return  &command.ResponseStatus{
		ExecAt: time.Now().Unix(),
	}
}

func NewDefaultCommandInput() *command.CommandInput {
	return &command.CommandInput{
	}
}


func NewDefaultExecResponse()  *command.ExecResponse {
	return  &command.ExecResponse{
		ResponseStatus: []*command.ResponseStatus{},  // 初始化返回数据
	}
}

func NewDefaultInput_Params()  *command.Input_Params {
	return  &command.Input_Params{
		Params: []string{},
	}
}




func(c  *CommandImpl) Exec(ctx  context.Context,req  *command.CommandInput) (*command.ExecResponse, error)  {
	collection:=c.mongDB.Collection("exec")
	searchRequest:=cmdbimpl.NewDefaultSearchRequest()
	searchRequest.Ipaddress=req.Resource.Ipaddress
	searchRequest.Region=req.Resource.Region
	searchRequest.SystemName=req.Resource.SystemName
	searchRequest.Id=req.Resource.Id
	resources,err:=c.CmdbSvc.Search(ctx, searchRequest)
	if err != nil {
		msg:=fmt.Sprintf("查询数据失败:%s",err)
		c.logger.Error().Msg(msg)
		return nil, err
	}
	execResource:=NewDefaultExecResponse()
	for  _,resource:=range  resources.Values {
		SshImpl:=util.NewDefaultSshImpl(resource)
		c.logger.Info().Msg(fmt.Sprintf("%s", resource.String()))
		c.logger.Info().Msg(fmt.Sprintf("%s", req.InputParams.String()))

		if req.InputParams.InputType==1 {
			req.InputParams.InputType=command.Input_Type_python
		}else {
			req.InputParams.InputType=command.Input_Type_shell
		}
		response:=NewDefaultResponseStatus()
		msg:=""
		c.logger.Info().Msg(fmt.Sprintf("%s 开始调用",req.InputParams.String()))
		code,output,err:=SshImpl.Ssh_Connect(req.InputParams)
		response.Id=DefaultRandom()
		if  err != nil {
			msg=fmt.Sprintf("Ssh 执行调用失败%s", err.Error())
		} else {
			msg=fmt.Sprintf("Ssh 执行调用成功")
		}
		response.Code=code 
		response.CommandOutput=output
		response.ExecUser=req.ExecUser
		response.Resource=resource
		response.Resource.Password=""
		response.CmdType=req.CmdType
		response.InputType=req.InputParams.InputType
		response.ErrorMsg=msg
		execResource.ResponseStatus = append(execResource.ResponseStatus, response)
		c.logger.Error().Msg(msg)
	}
	execResource.TotalNum=int64(len(execResource.ResponseStatus))
	if  execResource.TotalNum==0 {
		msg:=fmt.Errorf("无返回值输出")
		c.logger.Error().Msg(msg.Error())
		return nil,msg
	}
	for  _,resouce:=range execResource.ResponseStatus {
		_,err=collection.InsertOne(ctx,resouce)
		if err != nil {
			return nil, err
		}
	}
	return  execResource,nil
}



func(c  *CommandImpl) InsertParams(ctx context.Context, req *command.Input_Params) (*command.Input_Params, error){
	collection:=c.mongDB.Collection("params")
	req.CreateAt=time.Now().Unix()
	req.Id=DefaultRandom()
	if req.FilePath==""   && len(req.Params)==0{
		msg:=fmt.Errorf("参数错误")
		c.logger.Error().Msg(msg.Error())
		return nil,msg
	}
	_,err:=collection.InsertOne(ctx,req)
	if  err != nil {
		msg:=fmt.Errorf("数据插入失败%s",err.Error())
		c.logger.Error().Msg(msg.Error())
		return nil, msg
	}
	c.logger.Info().Msg(req.String())
	return req,nil 
}