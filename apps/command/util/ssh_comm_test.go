package  util_test  


import (
	"testing"
	"gitee.com/ChangPaoZhe/devops/apps/command/util"
	cmdbimpl  "gitee.com/ChangPaoZhe/devops/apps/cmdb/impl"
	"gitee.com/ChangPaoZhe/devops/apps/command"

	"gitee.com/ChangPaoZhe/devops/config"


)


var  sshImpl  *util.SshImpl
func TestSsh_Connect(t *testing.T) {
	input_param:=&command.Input_Params{
		InputType: command.Input_Type_python,
		FilePath: "",
		Params: []string{"/root/test.py","hello world"},
	}
	code,output,err:=sshImpl.Ssh_Connect(input_param)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(code,output)
}



func init(){
	fp:="/home/jasonbzhang/work/script/golang/devops/etc/application.toml"
	err := config.LoadConfigFromToml(fp)
	if err != nil {
		panic(err)
	}
	resouce:=cmdbimpl.NewDefaultResource()
	resouce.Username="root"
	resouce.Ipaddress="9.134.73.30"
	resouce.Port=36000
	resouce.Password="Zhangbing#123"
	sshImpl=util.NewDefaultSshImpl(resouce)
}