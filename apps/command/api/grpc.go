package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/command"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"google.golang.org/grpc"
)



type CommandGrpc struct {
	svc command.Commander
	command.UnimplementedRPCServer
}

func (c *CommandGrpc)Name() string {
	return command.App_Name
}


func (c *CommandGrpc)Init() error {
	c.svc = ioc.GetServer(c.Name()).(command.Commander)
	return nil
}
func (c *CommandGrpc)Registory(g grpc.ServiceRegistrar) {
	command.RegisterRPCServer(g, c)
}

func init(){
	ioc.RegisterGrpc(&CommandGrpc{})
}