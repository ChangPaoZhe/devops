package impl

import (
	"gitee.com/ChangPaoZhe/devops/apps/cmdb"
	"gitee.com/ChangPaoZhe/devops/apps/command"
	"gitee.com/ChangPaoZhe/devops/apps/cron"
	"gitee.com/ChangPaoZhe/devops/config"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/mongo"
)




type ImplCron struct{
	MongoDb   *mongo.Client
	logger    zerolog.Logger
	Command  command.Commander
	cron.UnimplementedRPCServer
	Cmdb  cmdb.RPCClienter
}


func (i *ImplCron)  Name() string {
	return cron.App_Name
}

func (i *ImplCron)  Init()  error {
	i.MongoDb=config.C.MongoDb.GetMongoDbInit()
	i.logger=config.C.Logs.Getlog(cron.App_Name)
	i.Command=ioc.GetServer(command.App_Name).(command.Commander)
	i.Cmdb=ioc.GetServer(cmdb.App_Name).(cmdb.RPCClienter)
	return  nil  
}

func init(){
	ioc.Register(&ImplCron{})
}

func (i *ImplCron) Start(){
	crontabs:=config.C.CrontabStatus
	go func(){
		crontabs.Crontab.AddFunc(crontabs.Spec, i.CrontabHealth)
		crontabs.Crontab.Start()
		select {}
	}()
}