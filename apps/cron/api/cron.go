package api 
import  (
	"gitee.com/ChangPaoZhe/devops/httpresponse"
	cronipml "gitee.com/ChangPaoZhe/devops/apps/cron/impl"
	"gitee.com/ChangPaoZhe/devops/apps/token"
	"github.com/gin-gonic/gin"

)

func (c *HttpCron) AddFunc(r *gin.Context) {
	req := cronipml.NewDefaultAddFuncParamsRequest()
	if err := r.BindJSON(req); err != nil {
		httpresponse.Failed(r, httpresponse.NewAuthFailed(err.Error()))
		return
	}
	resource, err := c.svc.AddFunc(r.Request.Context(), req)
	if err != nil {
		httpresponse.Failed(r, httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(r, resource)
}

func (c *HttpCron) InsertCront(r *gin.Context) {
	tkObj := r.Keys[token.TOKEN_GIN_KEY_NAME]
	tk, _ := tkObj.(*token.Token)
	req := cronipml.NewDefaultCrontabCommand()
	req.InserUser = tk.Username
	if err := r.BindJSON(req); err != nil {
		httpresponse.Failed(r, httpresponse.NewAuthFailed(err.Error()))
		return
	}
	resource, err := c.svc.InsertCront(r.Request.Context(), req)
	if err != nil {
		httpresponse.Failed(r, httpresponse.NewAuthFailed(err.Error()))
		return
	}
	httpresponse.Success(r, resource)
}
