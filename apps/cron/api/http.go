package api

import (
	"gitee.com/ChangPaoZhe/devops/apps/cron"
	"gitee.com/ChangPaoZhe/devops/ioc"
	"gitee.com/ChangPaoZhe/devops/middleware"
	"github.com/gin-gonic/gin"
)

type HttpCron struct {
	svc  cron.Croner
}


func (h *HttpCron) Name() string {
	return cron.App_Name
}

func  (h *HttpCron) Init() error {
	h.svc=ioc.GetServer(cron.App_Name).(cron.Croner)
	return  nil  
}
func (c *HttpCron) Registory(r gin.IRouter) {
	v1:=r.Group(cron.App_Name)
	v1.Use(middleware.NewTokenAuther().Auth)
	v1.POST("/addfun", c.AddFunc)
	v1.POST("/insertcron", c.InsertCront)
}


func init(){
	ioc.RegisterHttp(&HttpCron{})
}