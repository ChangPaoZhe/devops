// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.12.4
// source: apps/cron/pb/rpc.proto

package cron

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	RPC_AddFunc_FullMethodName     = "/cron.RPC/AddFunc"
	RPC_InsertCront_FullMethodName = "/cron.RPC/InsertCront"
)

// RPCClient is the client API for RPC service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RPCClient interface {
	AddFunc(ctx context.Context, in *AddFuncParamsRequest, opts ...grpc.CallOption) (*AddFuncParamsResponse, error)
	InsertCront(ctx context.Context, in *CrontabCommand, opts ...grpc.CallOption) (*CrontabCommand, error)
}

type rPCClient struct {
	cc grpc.ClientConnInterface
}

func NewRPCClient(cc grpc.ClientConnInterface) RPCClient {
	return &rPCClient{cc}
}

func (c *rPCClient) AddFunc(ctx context.Context, in *AddFuncParamsRequest, opts ...grpc.CallOption) (*AddFuncParamsResponse, error) {
	out := new(AddFuncParamsResponse)
	err := c.cc.Invoke(ctx, RPC_AddFunc_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rPCClient) InsertCront(ctx context.Context, in *CrontabCommand, opts ...grpc.CallOption) (*CrontabCommand, error) {
	out := new(CrontabCommand)
	err := c.cc.Invoke(ctx, RPC_InsertCront_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RPCServer is the server API for RPC service.
// All implementations must embed UnimplementedRPCServer
// for forward compatibility
type RPCServer interface {
	AddFunc(context.Context, *AddFuncParamsRequest) (*AddFuncParamsResponse, error)
	InsertCront(context.Context, *CrontabCommand) (*CrontabCommand, error)
	mustEmbedUnimplementedRPCServer()
}

// UnimplementedRPCServer must be embedded to have forward compatible implementations.
type UnimplementedRPCServer struct {
}

func (UnimplementedRPCServer) AddFunc(context.Context, *AddFuncParamsRequest) (*AddFuncParamsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddFunc not implemented")
}
func (UnimplementedRPCServer) InsertCront(context.Context, *CrontabCommand) (*CrontabCommand, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InsertCront not implemented")
}
func (UnimplementedRPCServer) mustEmbedUnimplementedRPCServer() {}

// UnsafeRPCServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RPCServer will
// result in compilation errors.
type UnsafeRPCServer interface {
	mustEmbedUnimplementedRPCServer()
}

func RegisterRPCServer(s grpc.ServiceRegistrar, srv RPCServer) {
	s.RegisterService(&RPC_ServiceDesc, srv)
}

func _RPC_AddFunc_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddFuncParamsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RPCServer).AddFunc(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RPC_AddFunc_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RPCServer).AddFunc(ctx, req.(*AddFuncParamsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RPC_InsertCront_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CrontabCommand)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RPCServer).InsertCront(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RPC_InsertCront_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RPCServer).InsertCront(ctx, req.(*CrontabCommand))
	}
	return interceptor(ctx, in, info, handler)
}

// RPC_ServiceDesc is the grpc.ServiceDesc for RPC service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RPC_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "cron.RPC",
	HandlerType: (*RPCServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AddFunc",
			Handler:    _RPC_AddFunc_Handler,
		},
		{
			MethodName: "InsertCront",
			Handler:    _RPC_InsertCront_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "apps/cron/pb/rpc.proto",
}
